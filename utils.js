require("dotenv").config();
const axios = require("axios");
module.exports.checkRecaptcha = async function(token, ip) {
  try {
    const secret = process.env.RECAPTCHA_SECRET_KEY;
    const res = await axios.post(
      `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${token}&remoteip=${ip}`
    );
    if (res.data.score > 0.5) return true;
    else return false;
  } catch (e) {
    console.error(e);
    return false;
  }
};
