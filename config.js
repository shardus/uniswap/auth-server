module.exports = {
  environment: "development",
  staticDirectory: "../uniswapdex/dist/_nuxt",
  viewDirectory: "../uniswapdex/dist",
  mainRoute: "/",
  port: {
    http: 3333,
    https: 443
  },
  keyLocation: "/etc/letsencrypt/live/uniswapdex.com"
};
